
const express = require('express')
const app = express()
var port = process.env.PORT || 8081;
const http = require('http').Server(app).listen(port)
const cors = require('cors') //New for Microservice
app.use(express.json())
app.use(cors()) //New for Microservice
const MongoClient = require('mongodb').MongoClient;
const mongourl = "mongodb+srv://cca-peesarir1:Kettering123@CCA-peesarir1.kt3x7.mongodb.net/cca-labs?retryWrites=true&w=majority";
const dbClient = new MongoClient(mongourl, {useNewUrlParser: true, useUnifiedTopology: true});
dbClient.connect(err=> {
    if (err) throw err;
    console.log("Connected to MongoDB Cluster")
})
let fields = {_id: false,
    zips: true,
    city: true,
    state_id: true,
    state_name: true,
    county_name: true,
    timezone: true};
app.use(express.static('static'))
app.use(express.urlencoded({extended: false}))
//app.listen(port, () =>
// //console.log('HTTP Server with Express.js is listening on port: '+ port))
// app.get('/', (req, res)=> {
//     res.sendFile(__dirname+ '/static/cca-form.html');
// })
app.get('/echo.php', function (req, res){
    var data = req.query.data
    res.send(data)
})
app.post('/echo.php', function (req, res){
    var data = req.body ['data']
    res.send(data)
})
app.get("/uscities-search", (req, res)=>{
    res.send("US City Microservice by Ram Reddy Peesari")
})
app.get('/uscities-search/:zips(\\d{1,5})', function (req,res) {
    const db = dbClient.db();
    let zipRegEx = new RegExp(req.params.zips);
    const cursor = db.collection("uscities").find({zips:zipRegEx}).project(fields);
    cursor.toArray(function(err, results){
        console.log(results);
        res.send(results);
    });
});
app.get('/uscities-search/:city', function (req, res){
    const db =dbClient.db();
    let cityRegEx = new RegExp(req.params.city,'i');
    const cursor = db.collection("uscities").find({city:cityRegEx}).project(fields);
    cursor.toArray(function(err, results){
        console.log(results);
        res.send(results);
    });    
});
const io = require('socket.io')(http);
console.log("socket.io server is running on port "+port);
app.get("/",(req,res)=>{
    res.sendFile(__dirname+'/static/chatclient/index.html')
})
io.on('connection',(socketclient)=> {
    console.log('A new client is connected!');
    socketclienthandler(socketclient)
})
function socketclienthandler(socketclient){
    socketclient.on("message", (data) => {
        consele.log('Data from a client: ' + data);
        io.emit("message",`${socketclient.id} says: ${data}`)
    });
}
app.post("/login", function(req,res){
    const db = dbClient.db();
    const {username,password} = req.body
    //some validations
    db.collection("users").findOne({username:username,password:password},(err, user)=>{
        if (user){
            user.status = true
            res.send(user) 
        }
        else{
            user={}
            user.status = false
            res.send(user)
        }
    });
})
app.post("/signup", function(req,res){
    const db = dbClient.db();
    const {username,password,email,fullname} = req.body
    //some validations
    db.collection("users").findOne({username:username},(err, user)=>{
        if (user){
            user.register = false 
            res.send(user) 
        }
        else{
            db.collection("users").insertOne(req.body,(err, user)=>{
                  user.register = true
                  res.send(user)
            })
        }
    });
})
