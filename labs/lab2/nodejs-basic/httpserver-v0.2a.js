var http=require("http");
var server=http.createServer(httphandler);
var port=process.env.port || 8080;
server.listen(port);
console.log("httpserver is running at port: "+port);
const fs=require('fs'), url=require('url');
function httphandler(request,response){
    console.log("Get an HTTP Request: "+ request.url);
    var fullpath=url.parse(request.url, true);
    var localfile= ".." + fullpath.pathname;
    console.log("Debug: Server's local file: "+localfile);
    fs.readFile(localfile,(error,filecontent) =>{
        response.writeHead(200,{'Content-Type': 'text/html'});
        response.write(filecontent);
        return response.end();
    });
}